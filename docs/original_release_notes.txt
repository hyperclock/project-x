
XOOPS 2.0.18.2 release notes
------------------------------------------------------------------------------

The XOOPS development team is pleased to announce the release of XOOPS 2.0.18.2.

In this release permission check ini imagemanager was fixed by dugris. A bug was fixed for custom session with regenerateId enabled.
Meanwhile to prevent further confusing information about 'fct' parameter in admin.php that might concern XOOPS users, the parameter is now filtered.

Read the full changelog for details.


System requirements
-----------------------------------

PHP
    Any PHP version >= 4.3 (PHP 4.2.x may work but is not officially supported)
MySQL
    MySQL server 3.23+
Web server
    Any server supporting the required PHP version (Apache highly recommended)


Downloading XOOPS
-----------------------------------

Your can get this release package from the sourceforge.net file repository.
Both .zip and .gz archives are provided.


Installing XOOPS
-----------------------------------

   1. Copy the content of the htdocs/ folder where it can be accessed by your server
   2. Ensure mainfile.php, cache, templates_c and uploads are writable by the webserver
   3. Access the folder where you installed the htdocs/ files using your web browser to launch the installation wizard


Upgrading from a previous version
-----------------------------------

Upgrading from XOOPS 2.0.18.1 (easy way)
   1. Get the update package from the sourceforge file repository
   2. Overwrite your existing files with the new ones

Upgrading from XOOPS 2.0.14/2.0.15/2.0.16/2.0.17/2.0.18.* (using the full package)
   1. Move the "upgrade" folder inside the "htdocs" folder (it's been kept out as it's not needed for full installs)
   2. Delete htdocs/mainfile.php, htdocs/install/, htdocs/cache/, htdocs/extras/, htdocs/template_c/, htdocs/themes/ and htdocs/uploads/
   3. Upload the content of the htdocs folder over your existing files
   4. Access <your.site.url>/upgrade/ with a browser, and follow the instructions
   5. Follow the instructions to update your database
   6. Delete the upgrade folder

Upgrading from any XOOPS ranging from 2.0.7 to 2.0.13.2 (using the full package):
   1. Move the "upgrade" folder inside the "htdocs" folder (it's been kept out as it's not needed for full installs)
   2. Delete htdocs/mainfile.php, htdocs/install/, htdocs/cache/, htdocs/template_c/, htdocs/themes/ and htdocs/uploads/
   3. Upload the content of the htdocs folder over your existing files
   4. Delete the following folders and files from your server (they belong to an old version):
          * class/smarty/core
          * class/smarty/plugins/resource.db.php
   5. Empty the templates_c folder (except index.html)
   6. Ensure the server can write to mainfile.php
   7. Access <your.site.url>/upgrade/ with a browser, and follow the instructions
   8. Write-protect mainfile.php again
   9. Delete the upgrade folder
  10. Update the "system" module from the modules administration interface


Files integrity check
-----------------------------------

The full XOOPS package is released with a script able to check if all the system files were correctly uploaded to the server. To use it, follow these instructions:

   1. Upload the checksum.php and checksum.md5 files located in the XOOPS package root to your XOOPS server folder (putting them next to mainfile.php).
   2. Execute checksum.php with your browser
   3. If necessary, re-upload the missing or corrupted system files
   4. Remove checksum.php and checksum.md5 from your server


Incompatibility with some modules
---------------------------------

Beginning with XOOPS 2.0.14, a change in the core template methods resulted in some incompatibilities with some modules that do not have their templates under (dirname)/templates/ as many XOOPS modules do. Please read extras/readme.txt for workaround instructions.


Modules
-----------------------------------

The packages do not contain any module apart from the system one. You are invited to browse the XOOPS modules repository to get some. Note: as a new repository is being built, the current repository is not up-to-date, PLEASE VISIT DEVELOPERS' WEBSITES TO MAKE SURE YOU ARE USING PROPER VERSION OF MODULES.

We also highly recommend the installation of the Protector module which will bring additional protection and logging capabilities to your site.


XOOPS Development Group