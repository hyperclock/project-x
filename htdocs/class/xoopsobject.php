<?php
// $Id: xoopsobject.php 2 2005-11-02 18:23:29Z skalpa $
if (!defined('XOOPS_ROOT_PATH')) {
	exit();
}
/**
 * this file is for backward compatibility only
 * @package kernel
 **/
/**
 * Load the new object class 
 **/
require_once XOOPS_ROOT_PATH.'/kernel/object.php';
?>
