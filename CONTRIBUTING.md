# Contributing to XOOPS CMS Project

We welcome your contributions to [Project X](https://gitlab.com/hyperclock/project-x) on [GitLab](https://gitlab.com/hyperclock/project-x)!

We use a [fork and pull](https://www.atlassian.com/git/tutorials/comparing-workflows/forking-workflow) workflow on GitLab that allows anyone to contribute changes. You don't need any special permissions, just a GitLab account. This is a lot easier to manage if you follow these basic steps:

- [Fork the Project X repository]() and work from that
- Create a [topic branch](http://git-scm.com/book/en/v2/Git-Branching-Branching-Workflows#Topic-Branches) for your changes, and keep all changes on that branch focused on a specific topic.
- Please consider updating the [unit tests](https://phpunit.de/getting-started.html) to include and verify your changes
- Once your changes are complete, commit your topic branch to your fork and create a [pull request (PR)]()
- Be prepared to discuss your changes. We will review and test each PR, and may need clarification or changes. Once we have completed the review, you changes will be incorporated into *Project X*.

# Licensing

By contributing code you agree to license your contribution under the [GNU General Public License, Version 2 or any later version.](http://www.gnu.org/licenses/gpl-2.0.html)

