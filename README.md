# Project X
Project X is a rewrite of an antique version of a well known Content Management System.


## About
I like the old XOOPS distributions, so I decided to take the 2.0.18.2 (last of the 2.0.x series) version of XOOPS.
Upgrading,fixing, changing, modding ... it to become a modern CMS.


## Plans
All development plans can be found in the [MILESTONES AREA](../milestones).


## Support
Discussions, Problems, Bugs, Suggestions, Etc. take place in the [ISSUES AREA](../issues)

## Documentation
There will be some documents being placed in the [WIKI AREA](..wikis/home)

## License
[GPLv2](LICENSE)